arrow-black.svg is based on arrow-blue.svg from http://openclipart.org/detail/77329/arrow-blue-by-shokunin
logo is leftover_bacon_Camera_Lens.svg from http://openclipart.org/detail/8099/camera-lens-by-leftover_bacon

The following icons were created by me:
torch-on.svg
torch-off.svg
shutter.svg
filter-black-white.svg
exposure.svg
filter-negative.svg
filter-solarize.svg
filter-vivid.svg
filter-sepia.svg
filter-none.svg

The rest is based on icons from https://github.com/nemomobile/nemo-theme-default lgpl part
